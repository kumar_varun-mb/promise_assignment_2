const fs = require('fs');
const path = require('path');

function readFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, `data/${fileName}`), 'utf-8',
            (err, data) => {
                if (err)
                    reject(err);
                else
                    resolve(data);
            })
    })
}
module.exports = readFile;