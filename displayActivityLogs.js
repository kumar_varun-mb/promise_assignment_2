const readFile = require('./readFile');
const logFile = 'activity.json';

function displayActivityLogs(userName) {
    readFile(logFile)
    .then((data) => {
        data = JSON.parse(data);
        console.log(userName+': '+data[userName]);
    })
}
module.exports = displayActivityLogs;