const fs = require('fs');
const path = require('path');

function writeFile(fileName, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(__dirname, `data/${fileName}`), data, 'utf-8',
            (err) => {
                if (err)
                    reject(err);
                else
                    resolve();
            })
    })
}
module.exports = writeFile;