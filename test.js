const books = require('./data/PromiseProblem');
const signIn = require('./signIn');
const getBooks = require('./getBooks');
const logData = require('./logData');
const displayActivityLogs = require('./displayActivityLogs');
const writeFile = require('./writeFile');
const logFile = 'activity.json';

// Initiate an empty object in activity.json
writeFile(logFile, '{}');

//Now the promise chaining starts
const userName = 'Chandler';
signIn(userName)
.then(res => {
    console.log('Sign In Success')
    return getBooks(userName, books);
})
.then(bookData => {
  console.log('Get Books Success');  
})
.catch(err => {
    console.log(err);
})