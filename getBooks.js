const logData = require('./logData');

function getBooks(userName, books) {
    return new Promise((resolve, reject) => {
        const randomNumber = Math.floor(Math.random() * 2);
        if (randomNumber == 1) {
            let booksList = books.reduce((acc, curr) => {
                acc.push(curr.name);
                return acc;
            }, []);
            logData(userName, 'GetBooks - Success');
            resolve(booksList);
        }
        else{
            logData(userName, 'GetBooks - Failure');
            reject('Error getting Books\nReason: Service Error\nResponse Code: 500');
        }
    });
}
module.exports = getBooks;