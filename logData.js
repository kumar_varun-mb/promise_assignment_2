const fs = require('fs');
const path = require('path');
// const readFile = require('./readFile.js');
// const writeFile = require('./writeFile.js');
// const logFile = 'activity.json';

function logData(username, data) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {

        
        fs.readFile(path.join(__dirname, 'data/activity.json'), 'utf-8',
            (err, obj) => {
                if (err)
                    reject(err);
                else {
                    obj = JSON.parse(obj);
                    if (!(obj[username]))
                        obj[username] = [];
                    obj[username].push(data);
                    // writeFile(logFile, JSON.stringify(obj, null, "\t"));
                    fs.writeFile(path.join(__dirname, 'data/activity.json'), JSON.stringify(obj, null, "\t"), 'utf-8',
                        (err) => {
                            if (err)
                                throw err;
                        })
                }
            })
        },1000);
    })
}
module.exports = logData;

