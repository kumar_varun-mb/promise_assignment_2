const logData = require('./logData');

function signIn(userName) {
    return new Promise((resolve, reject) => {
        let seconds = new Date().getSeconds();
        if (seconds > 30) {
            logData(userName, 'Sign In - Success');
            resolve(seconds);
        }
        else {
            logData(userName, 'Sign In - Failure');
            reject('Sign In Error\nReason: Auth Failure\nResponse Code: 401');
        }
    })
}
module.exports = signIn;